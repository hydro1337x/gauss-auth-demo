'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

/** User Routs*/

//POST

/**
 * @api {post} /auth/register Create new user
 * @apiDescription Creates a new user and sends a verification email.
 * @apiGroup Auth
 * @apiParamExample {json} Input
 * {
 *     "username" : "test",
 *     "email" : "test@domain.com",
 *     "password" : "123456"
 * }
 * @apiSuccessExample {json} Success
 * {
 *   "username": "test",
 *   "email": "test@domain.com",
 *   "password": "$2a$10$.hvCsppGxk5tMQ9YqNJUq.XvPtP4bXi8S4D4uRJJcv7KqoBBcfNu2",
 *   "refreshToken": "0eab751e-d71b-41c8-915b-4561fec5090b",
 *   "created_at": "2019-07-17 10:35:09",
 *   "updated_at": "2019-07-17 10:35:09",
 *   "id": 1
 *   }
 */

Route.post('/auth/register', 'AuthController.register').middleware('registrationValidator')

/**
 * @api {post} /auth/login Sign in
 * @apiDescription
 * This route enables to sign the user in using a registered email and password.
 * If the user did not verify its account, on log in a 401 unauthorized response will be sent.
 * If the user did verify its account, on log in a refresh and access token will be sent instead in the response.
 * @apiGroup Auth
 * @apiParamExample {json} Input
 * {
 *     "email" : "test@domain.com",
 *     "password" : "123456"
 * }
 * @apiSuccessExample {json} Success
 * {
 *   "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywiaWF0IjoxNTYzMzUzMjk3LCJleHAiOjE1NjMzNTY4OTd9.7M9WjhYcL7zD9PBUYa49zDTxjV3hC-7zkVPx4oaChvU",
 *   "refreshToken": "0eab751e-d71b-41c8-915b-4561fec5090b"
 * }
 */

Route.post('/auth/login', 'AuthController.login').middleware('loginValidator')

//PATCH

/**
 * @api {patch} /auth/password/update Change password
 * @apiDescription
 * This route enables a signed in user to change its existing password to a new one, considering that the user provides its old password.
 * The password needs to be at least 6 characters long.
 * @apiGroup Auth
 * @apiParamExample {json} Input
 * {
 *	"oldPassword" : "myOldPassword",
 *	"repeatedOldPassword": "myOldPassword",
 *	"newPassword": "myNewPassword"
 * }
 * @apiSuccess {String} Success Successfully updated password
 */

Route.patch('/auth/password/update', 'AuthController.updatePassword').middleware('updatePasswordValidator').middleware('authenticator')

/**
 * @api {patch} /auth/password/new?token=accessToken Create new password
 * @apiDescription
 * This route enables the user to set a new password after a forgot password email was sent.
 * This route also changes the user's refresh token.
 * The new password should be at least 6 characters long.
 * @apiGroup Auth
 * @apiParam {String} token Access token. Query param.
 * @apiSuccess {String} Success New password successfully set
 */

Route.patch('auth/password/new', 'AuthController.setNewPassword').middleware('newPasswordValidator')

//GET

/**
 * @api {get} /auth/verification?token=accessToken Verify account
 * @apiDescription
 * Non verified user's can not log in to their account.
 * Should not be used by the frontend.
 * @apiGroup Auth
 * @apiParam {String} accessToken Access token. Query param.
 * @apiSuccess {String} Success Successfully verified!
 */

Route.get('/auth/verification', 'AuthController.verify')

/**
 * @api {get} /auth/verification/resend Re-send verification email
 * @apiGroup Auth
 * @apiParamExample {json} Input
 * {
 *	"email" : "test@domain.com"
 * }
 * @apiSuccess {String} Success A verification email has been sent to your inbox.
 */

Route.get('/auth/verification/resend', 'AuthController.resend').middleware('emailValidator')

/**
 * @api {get} /auth/refresh?token=refreshToken Refreshes access token
 * @apiGroup Auth
 * @apiParam {String} refreshToken Refresh token. Query param.
 * @apiSuccessExample {json} Success
 * {
 *  "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNTYzNjM1NzQ0LCJleHAiOjE1NjM2MzkzNDR9.YlMojCoAyVdk-t2w8oqNURuXtlr7RAJ9ZADO4eFZgpw"
 * }
 */

Route.get('/auth/refresh', 'AuthController.refreshAccessToken')

/**
 * @api {get} /auth/password/forgot Forgot password
 * @apiDescription
 * Sends a password reset form to your email.
 * @apiGroup Auth
 * @apiParamExample {json} Input
 * {
 *	"email" : "test@domain.com"
 * }
 * @apiSuccess {String} Success Password reset form was sent to your inbox
 */

Route.get('/auth/password/forgot', 'AuthController.forgotPassword').middleware('emailValidator')


/** Device Routs*/

//POST

/**
 * @api {post} /device/add Add device
 * @apiDescription
 * Adds a new device to the list of devices on your account.
 * @apiGroup Device
 * @apiParamExample {json} Input
 * {
 *  "name" : "Test device",
 *	"push_token" : "erG0IL4mCkA:APA91bGnp8Y2a5CdIK6XFCqq7umd1gPtw8ATzhHzIyM--wL38i5Brv-tpaEAkp8xOExtbwrhk8kSkgrs9trVnh6n4kAXwS85CkwrgcW2waffawFM28sVOswNC1kQOKhaz5MOVWV449"
 * }
 * @apiHeader {String} access-token The access token received on login.
 * @apiSuccessExample {json} Success
 * {
 *  "name": "Test device",
 *  "user_id": 1,
 *  "push_token": "erG0IL4mCkA:APA91bGnp8Y2a5CdIK6XFCqq7umd1gPtw8ATzhHzIyM--wL38i5Brv-tpaEAkp8xOExtbwrhk8kSkgrs9trVnh6n4kAXwS85CkwrgcW2waffawFM28sVOswNC1kQOKhaz5MOVWV449",
 *  "created_at": "2019-07-20 17:46:58",
 *  "updated_at": "2019-07-20 17:46:58",
 *  "id": 2
 * }
 */

Route.post('/device/add', 'DeviceController.addDevice').middleware('authenticator')

/**
 * @api {post} /device/notification/send Send push notification
 * @apiGroup Device
 * @apiParamExample {json} Input
 * {
 *  "device_id" : "1",
 *    "data": {
 *      "fcm_number_key": "+385984228512",
 *      "fcm_message_key": "Awesome test message"
 *   }
 * }
 * @apiHeader {String} access-token The access token received on login.
 * @apiSuccessExample {json} Success
 * {
 *  "multicast_id": 7045654019996604000,
 *  "success": 1,
 *  "failure": 0,
 *  "canonical_ids": 0,
 *  "results": [
 *      {
 *         "message_id": "0:1563638502721411%3fa54ef8f9fd7ecd"
 *      }
 *  ]
 * }
 */

Route.post('/device/notification/send', 'DeviceController.sendPushNotification').middleware('authenticator')

//GET

/**
 * @api {get} /device/get/all Fetch devices
 * @apiDescription
 * Fetches all devices associated to a account if no parameters were specified.
 * If a search string is specified it will fetch devices based on it.
 * @apiGroup Device
 * @apiParam {String} searchStr Search string. Query param.
 * @apiHeader {String} access-token The access token received on login.
 * @apiSuccessExample {json} Success
 * [
 *  {
 *       "id": 1,
 *       "name": "Test device 1",
 *       "push_token": "erG0IL4mCkA:APA91bGnp8Y2a5CdIK6XFCqq7umd1gPtw8ATzhHzIyM--wL38i5Brv-tpaEAkp8xOExtbwrhk8kSkgrs9trVnh6n4kAXwS85CkwrWCph1YZagcW2FM28sVOswNC1kQOKhaz5MOVWV449",
 *       "user_id": 1,
 *       "created_at": "2019-07-16 12:55:08",
 *       "updated_at": "2019-07-16 12:55:08"
 *   },
 *  {
 *      "id": 2,
 *      "name": "Test device 2",
 *      "push_token": "erG0IL4mCkA:APA91bGnp8Y2a5CdIK6XFCqq7umd1gPtw8ATzhHzIyM--wL38i5Brv-tpaEAkp8xOExtbwrhk8kSkgrs9trVnh6n4kAXwS85CkwrgcW2FM28sVOswNC1kQOKhaz5MOVWV449",
 *      "user_id": 1,
 *      "created_at": "2019-07-16 14:22:38",
 *      "updated_at": "2019-07-16 14:22:38"
 *  }
 * ]
 */

Route.get('/device/get/all', 'DeviceController.getDevices').middleware('authenticator')

//DELETE

/**
 * @api {delete} /device/delete Delete device
 * @apiGroup Device
 * @apiParamExample {json} Input
 * {
 *  "id": "1"
 * }
 * @apiHeader {String} access-token The access token received on login.
 * @apiSuccess {String} Success Successfully deleted device with id: 1
 */

Route.delete('/device/delete', 'DeviceController.deleteDevice').middleware('authenticator')





