'use strict'

const {validate} = use('Validator')

class UpdatePasswordValidator {

  async handle (ctx, next) {

    const { request, response } = ctx

    const input = request.post()

    const rules = {
      oldPassword : 'required|min:6',
      repeatedOldPassword : 'required|min:6',
      newPassword : 'required|min:6'
    }

    const messages = {
      'oldPassword.required' : 'You must provide your old password',
      'oldPassword.min' : 'Your old password must be at least 6 characters long',
      'repeatedOldPassword.required' : 'You must repeat your old password',
      'repeatedOldPassword.min' : 'Your repeated old password must be at least 6 characters long',
      'newPassword.required' : 'You must provide a new password',
      'newPassword.min' : 'Your new password must be at least 6 characters long'
    }

    const validation = await validate(input, rules, messages)

    if (validation.fails()) {
      return response.badRequest(validation.messages())
    }

    if (input.oldPassword != input.repeatedOldPassword) {
      return response.badRequest('Your old password and repeated password do not match')
    }

    ctx.passwords = {
      'oldPassword' : input.oldPassword,
      'newPassword' : input.newPassword
    }

    await next()
  }
}

module.exports = UpdatePasswordValidator
