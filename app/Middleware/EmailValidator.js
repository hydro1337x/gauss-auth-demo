'use strict'

const {validate} = use('Validator')

class EmailValidator {

  async handle ( ctx, next) {

    const { response, request } = ctx

    const {email} = request.post()

    const rules = {
      email: 'required|email'
    }

    const messages = {
      'email.required': 'You must provide a email address.',
      'email.email': 'You must provide a valid email address.'
    }

    const validation = await validate(email, rules, messages)

    if (validation.fails()) {
      return response.badRequest(validation.messages())
    }

    ctx.email = email

    await next()
  }
}

module.exports = EmailValidator
