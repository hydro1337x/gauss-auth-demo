'use strict'

const {validate} = use('Validator')

class NewPasswordValidator {

  async handle (ctx, next) {

    const { request, response } = ctx

    const input = request.post()

    const rules = {
      newPassword : 'required|min:6',
      repeatedNewPassword : 'required|min:6'
    }

    const messages = {
      'repeatedNewPassword.required' : 'You must repeat your new password',
      'repeatedNewPassword.min' : 'Your repeated new password must be at least 6 characters long',
      'newPassword.required' : 'You must provide a new password',
      'newPassword.min' : 'Your new password must be at least 6 characters long'
    }

    const validation = await validate(input, rules, messages)

    if (validation.fails()) {
      return response.badRequest(validation.messages())
    }

    if (input.newPassword != input.repeatedNewPassword) {
      return response.badRequest('Your new password and repeated password do not match')
    }

    ctx.password = input.newPassword

    await next()
  }
}

module.exports = NewPasswordValidator
