'use strict'

const {validate} = use('Validator')

class LoginValidator {

  async handle (ctx, next) {

    const {request, response} = ctx

    const user = request.post()

    const loginInput = { 'email': user.email, 'password': user.password }

    const rules = {
      email: 'required|email',
      password: 'required|min:6'
    }

    const messages = {
      'email.required': 'You must provide a email address.',
      'email.email': 'You must provide a valid email address.',
      'password.required': 'You must provide a password',
      'password.min': 'Your password must be at least 6 characters long'
    }

    const validation = await validate(loginInput, rules, messages)

    if (validation.fails()) {
      return response.badRequest(validation.messages())
    }

    ctx.loginInput = loginInput


    await next()
  }
}

module.exports = LoginValidator
