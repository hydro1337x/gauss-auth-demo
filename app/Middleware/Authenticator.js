'use strict'

const jwt = require('jsonwebtoken')
const Env = use('Env')

class Authenticator {

  async handle (ctx, next) {

    const { request, response } = ctx

    const token = request.header('access-token')

    if (!token) {
      return response.unauthorized('Access denied')
    }

    const privateKey = Env.get('APP_KEY')

    try {
      var verified = jwt.verify(token, privateKey)
    } catch (error) {
      return response.badRequest(error)
    }

    ctx.payload = verified

    await next()
  }
}

module.exports = Authenticator
