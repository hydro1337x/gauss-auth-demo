'use strict'

const {validate} = use('Validator')

class RegistrationValidator {

  async handle (ctx, next) {
    const {request, response} = ctx
    const userInfo = request.post()

    const rules = {
      username: 'required|unique:users,username',
      email: 'required|email|unique:users,email',
      password: 'required|min:6'
    }

    const messages = {
      'username.required': 'You must provide an username',
      'username.unique': 'This username is already registered',
      'email.required': 'You must provide a email address.',
      'email.email': 'You must provide a valid email address.',
      'email.unique': 'This email is already registered.',
      'password.required': 'You must provide a password',
      'password.min': 'Your password must be at least 6 characters long'
    }

    const validation = await validate(request.all(), rules, messages)

    if (validation.fails()) {
      return response.badRequest(validation.messages())
    }

    ctx.userInfo = userInfo

    await next()
  }
}

module.exports = RegistrationValidator
