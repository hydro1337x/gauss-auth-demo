'use strict'

const User = use('App/Models/User')
const Device = use('App/Models/Device')
const Env = use('Env')

const gcm = require('node-gcm')

class DeviceController {

  async addDevice( {request, payload, response} ) {

    try {
      var user = await User.findOrFail(payload.id)
    } catch (error) {
      return response.badRequest('fwaifwaf')
    }

    const { name, push_token } = request.post()

    const user_id = user.id

    try {
      var device = await Device.create({name, user_id, push_token})
    } catch (error) {
      return response.badRequest(error)
    }

      return  device
  }

  async getDevices( {request, payload, response} ) {

    const q = Device.query()

    const { searchStr = false } = request.get()

    if (!searchStr){
      q.where('user_id', payload.id)
    } else {
      q.where('user_id', payload.id).where('name', 'like', `%${searchStr}%`)
    }

    try {
      var devices = await q.fetch()
    } catch (error) {
      return response.badRequest(error)
    }

    response.ok(devices)

  }

  async deleteDevice( { request, payload, response} ) {

    const userID = payload.id

    const deviceID = request.post().id

    try {
      var result = await Device.query().where('user_id', userID).where('id', deviceID).delete()
    } catch (error) {
      return response.badRequest(error)
    }

    if (!result) {
      return response.badRequest('Could not delete device')
    }

    response.ok(`Successfully deleted device with id: ${deviceID}`)

  }

  async sendPushNotification( { request, payload, response } ) {

    const { device_id, data} = request.post()

    const userID = payload.id

    try {
      var device = await Device.query().select('*').where('user_id', userID).where('id', device_id).firstOrFail()
    } catch (error) {
      return response.badRequest(error)
    }


    const gcmMessage = new gcm.Message({
      data
    })

    const sender = new gcm.Sender(Env.get('API_KEY'))

    const registrationTokens = [device.push_token]

    try {
      const pushResponse = await new Promise((resolve, reject) => {
        sender.send(gcmMessage, {registrationTokens : registrationTokens}, function (err, res) {
          if(err) return reject(err)
          resolve(res)
        })
      })

      return pushResponse
    } catch(err) {
      return response.badRequest(err)
    }


  }

}

module.exports = DeviceController
