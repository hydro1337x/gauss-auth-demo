'use strict'

const User = use('App/Models/User')
const Hash = use('Hash')
const Env = use('Env')
const Mail = use('Mail')
const Database = use('Database')
const uuidv4 = require('uuid/v4');
const jwt = require('jsonwebtoken')

class AuthController {

  async register({response, userInfo}) {

    const {username, email, password} = userInfo

    const refreshToken = uuidv4()

    try {
      var user = await User.create({username, email, password, refreshToken})

    } catch (err) {
      return response.badRequest(err)
    }

    const emailKey = Env.get('EMAIL_KEY')

    const verificationToken = jwt.sign({"id" : user.id}, emailKey, { expiresIn: 15 * 60 }) // JTW Email Verification Token expiration time set to 15 minutes

    const verificationURL = Env.get('VERIFICATION_URL')

    await Mail.send('verification', { url: verificationURL + verificationToken}, (message) => {
      message
        .from('f33973611e-480220@inbox.mailtrap.io')
        .to(user.email)
        .subject('Welcome')
    })

    response.created(user)

  }

  async login({response, loginInput}) {
    let user

    try {
       user = await User.findByOrFail('email', loginInput.email)
    } catch {
      return response.badRequest('Invalid email adress')
    }

    if (!user.isVerified) {
      return response.unauthorized('Please verify your email')
    }

    const isSame = await Hash.verify(loginInput.password, user.password)

    if (!isSame) {
      return response.badRequest('Invalid password')
    }

    const privateKey = Env.get('APP_KEY')

    const accessToken = jwt.sign({"id" : user.id}, privateKey, { expiresIn: 60 * 60 })

    const refreshToken = user.refreshToken

    response.ok({
      'accessToken': accessToken,
      'refreshToken': refreshToken
    })
  }

  async verify({response, request}) {

    const verificationToken = request.get().token

    if (!verificationToken) {
      return response.badRequest('Invalid Token')
    }
    const emailKey = Env.get('EMAIL_KEY')

    let user

    try {
      user = jwt.verify(verificationToken, emailKey)
    } catch (error) {
      return response.badRequest(error)
    }

    const result = await Database.table('users').where('id', user.id).update('isVerified', true) //update returns 1 if successful

    if (!result) {
      return response.badRequest('Verification error')
    }
    response.ok('Successfully verified!')
  }

  async resend({ response, email}) {

    let user

    try {
      user = await User.findByOrFail('email', email)
    } catch (error) {
      response.badRequest(error)
    }

    const privateKey = Env.get('EMAIL_KEY')

    const verificationURL = Env.get('VERIFICATION_URL')

    const verificationToken = jwt.sign({ "id" : user.id}, privateKey, { expiresIn: 15 * 60 })

    await Mail.send('verification', { url: verificationURL + verificationToken}, (message) => {
      message
        .from('f33973611e-480220@inbox.mailtrap.io')
        .to(user.email)
        .subject('Welcome')
    })

    response.ok('A verification email has been sent to your inbox.')

  }

  async updatePassword({ payload, passwords, response }) {

    try {
      var user = await User.findOrFail(payload.id)
    } catch (error) {
      return response.badRequest(error)
    }

    if (!user.isVerified) {
      return response.unauthorized('Please verify your email')
    }

    const isSame = await Hash.verify(passwords.oldPassword, user.password)

    if(!isSame) {
      return response.badRequest('Invalid old password')
    }

    const newHashedPassword = await Hash.make(passwords.newPassword)

    const result = await Database.table('users').where('id', user.id).update('password', newHashedPassword)

    if (!result) {
      response.badRequest('Could not update password')
    }

    response.ok('Successfully updated password')

  }

  async refreshAccessToken ({ request, response }) {

    const refreshToken = request.get().token

    try {
      var user = await User.findByOrFail('refreshToken', refreshToken)
    } catch (error) {
      return response.badRequest('Could not find user with sent refresh token')
    }

    const privateKey = Env.get('APP_KEY')

    const accessToken = jwt.sign({'id': user.id}, privateKey, { expiresIn: 60 * 60 })

    response.ok({
      'accessToken': accessToken
    })
  }

  async forgotPassword( { email, response} ) {

    try {
      var user = await User.findByOrFail('email', email)
    } catch (error) {
      return response.badRequest(error)
    }

    const privateKey = Env.get('APP_KEY')

    const accessToken = jwt.sign({'id': user.id}, privateKey, { expiresIn: 15 * 60 })

    const passwordResetURL = Env.get('RESET_PASSWORD_URL')

    await Mail.send('reset-password', { url: passwordResetURL + accessToken}, (message) => {
      message
        .from('f33973611e-480220@inbox.mailtrap.io')
        .to(user.email)
        .subject('Welcome')
    })

    response.ok('Password reset form was sent to your inbox')

  }

  async setNewPassword( {password, response, request}) {

    const accessToken = request.get().token

    const privateKey = Env.get('APP_KEY')

    try {
      var payload = jwt.verify(accessToken, privateKey)
    } catch (error) {
      return response.badRequest(error)
    }

    try {
      var user = await User.findOrFail(payload.id)
    } catch (error) {
      return response.badRequest(error)
    }

    const refreshToken = uuidv4()

    const newHashedPassword = await Hash.make(password, privateKey)

    const result = await Database.table('users').where('id', user.id).update('password', newHashedPassword).update('refreshToken', refreshToken)

    if(!result) {
      return response.badRequest('Could not set new password')
    }

    response.ok('New password successfully set')

  }

}

module.exports = AuthController
